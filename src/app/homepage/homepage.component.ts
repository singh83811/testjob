import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss']
})


export class HomepageComponent {
  public addForm: FormGroup;
  public submittedForm: boolean;
  formData: any[] = [];
  buttonLabel = 'Add'
  editId: number;
  filterValue: any;

  constructor(
    private formBuilder: FormBuilder
  ) { }


  ngOnInit() {
    this.createForm();
    this.getData();
  }

  createForm() {
    this.addForm = this.formBuilder.group({
      id: [],
      name: ['', [Validators.required]],
      department: ['', [Validators.required]],
      salary: [, [Validators.required]],
    });
  }

  getData() {
    const savedData = localStorage.getItem('formData');
    this.formData = savedData ? JSON.parse(savedData) : [];
  }

  deleteData(id: number) {
    const updatedData = this.formData.filter(item => item.id !== id);
    localStorage.setItem('formData', JSON.stringify(updatedData));
    location.reload();
  }

  editData(id: number) {
    this.buttonLabel = 'Edit';
    this.editId = id;
    const findData = this.formData.filter(item => item.id === id);
    findData.forEach((item: any) => {
      this.addForm.patchValue({
        name: item?.name,
        department: item?.department,
        salary: item?.salary,
      });

    })
  }

  setFilter(key) {
    this.filterValue = key;
  }

  searchFilter(event) {
    const searchTerm = event.value
    const data = this.formData
    if (this.filterValue == 'department') {
      if (searchTerm) {
        this.formData = data.filter(item =>
          item.department.toLowerCase().includes(searchTerm.toLowerCase())
        );
      } else {
        this.formData = JSON.parse(localStorage.getItem('formData'));
      }
    }
  }

  sortData(amount: string) {
    this.formData.sort((a, b) => a[amount] - b[amount]);
  }

  submit(value) {
    // stop here if form is invalid
    this.submittedForm = true;
    if (this.addForm.invalid) {
      return;
    }
    if (value == 'add') {
      this.addForm.value.id = this.formData.length + 1;
      this.formData.push(this.addForm.value);
      localStorage.setItem('formData', JSON.stringify(this.formData));
      location.reload();
    }

    if (value == 'edit') {
      const updatedData = this.formData.filter(item => item.id === this.editId);
      updatedData.forEach((item: any) => {
        item.name = this.addForm.value.name;
        item.department = this.addForm.value.department;
        item.salary = this.addForm.value.salary;
      });
      localStorage.setItem('formData', JSON.stringify(this.formData))
      location.reload();
    }
  }
}
